import React from 'react'
import Svg, { Defs, Circle, Use, ClipPath, G, Path } from 'react-native-svg'

const SvgComponent = props => (
  <Svg height={props.height} viewBox="-27 24 100 100" width={props.width} {...props}>
    <Defs>
      <Circle cx={23} cy={74} id="prefix__a" r={50} />
    </Defs>
    <Use fill="#F5EEE5" overflow="visible" xlinkHref="#prefix__a" />
    <ClipPath id="prefix__b">
      <Use overflow="visible" xlinkHref="#prefix__a" />
    </ClipPath>
    <G clipPath="url(#prefix__b)">
      <Defs>
        <Path
          d="M36 95.9c0 4 4.7 5.2 7.1 5.8 7.6 2 22.8 5.9 22.8 5.9 3.2 1.1 5.7 3.5 7.1 6.6v9.8H-27v-9.8c1.3-3.1 3.9-5.5 7.1-6.6 0 0 15.2-3.9 22.8-5.9 2.4-.6 7.1-1.8 7.1-5.8V85h26v10.9z"
          id="prefix__c"
        />
      </Defs>
      <Use fill="#E6C19C" overflow="visible" xlinkHref="#prefix__c" />
      <ClipPath id="prefix__d">
        <Use overflow="visible" xlinkHref="#prefix__c" />
      </ClipPath>
      <Path
        clipPath="url(#prefix__d)"
        d="M23.2 35h.2c3.3 0 8.2.2 11.4 2 3.3 1.9 7.3 5.6 8.5 12.1 2.4 13.7-2.1 35.4-6.3 42.4-4 6.7-9.8 9.2-13.5 9.4H23h-.1c-3.7-.2-9.5-2.7-13.5-9.4-4.2-7-8.7-28.7-6.3-42.4 1.2-6.5 5.2-10.2 8.5-12.1 3.2-1.8 8.1-2 11.4-2h.2z"
        fill="#D4B08C"
      />
    </G>
    <Path
      d="M22.6 40c19.1 0 20.7 13.8 20.8 15.1 1.1 11.9-3 28.1-6.8 33.7-4 5.9-9.8 8.1-13.5 8.3h-.5c-3.8-.3-9.6-2.5-13.6-8.4-3.8-5.6-7.9-21.8-6.8-33.8C2.3 53.7 3.5 40 22.6 40z"
      fill="#F2CEA5"
    />
  </Svg>
)

export default SvgComponent
