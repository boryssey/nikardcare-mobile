import * as types from './AuthTypes';

export function loginUser(user) {
  return{
    type: types.LOGIN_USER,
    payload: user
  }
}
export function loginError(error) {
  return{
    type: types.LOGIN_ERROR,
    payload: error
  }
}

export function logoutUser(){
  return{
    type: types.LOGOUT_USER,
  }
}

export function refreshToken(token, refreshToken){
  return{
    type: types.REFRESH_TOKEN,
    payload: {token, refreshToken}
  }
}
