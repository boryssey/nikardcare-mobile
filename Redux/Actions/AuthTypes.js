export const LOGIN_USER = 'LOGIN_USER'
export const LOGOUT_USER = 'LOGOUT_USER'
export const LOGIN_ERROR = 'LOGIN_ERROR'
export const REFRESH_TOKEN = 'REFRESH_TOKEN'
