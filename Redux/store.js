import {createStore, applyMiddleware} from 'redux'
import logger from 'redux-logger'
import * as types from './Actions/AuthTypes'
import jwtDecode  from 'jwt-decode'
const initialState = {
  isAuth: false,
  user_tokens: null,
  error: null,
  user:null,
}



function loginReducer (state = initialState, action) {
  switch(action.type) {
    case types.REFRESH_TOKEN:
      return {
        ...state,
        isAuth: true,
        user_tokens: {token: action.payload.token, refreshToken: action.payload.refreshToken},
        user: jwtDecode(action.payload.token)
      }
    case types.LOGIN_USER:
      return {
        ...state,
        isAuth: true,
        user_tokens: action.payload,
        user: jwtDecode(action.payload.token)
      }
    case types.LOGOUT_USER:
      return {
        ...state,
        isAuth: false,
        user_tokens: null,
        user: null
      }
    case types.LOGIN_ERROR:
      return {
        isAuth: false,
        user_tokens: null,
        error: action.payload
      }
    default:
      return{
        ...state
      }
  }
}


const store = createStore(loginReducer, applyMiddleware(logger))
export default store;
