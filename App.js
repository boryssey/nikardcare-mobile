import React from 'react'
import {View, Text, StyleSheet} from 'react-native'
import {Provider, connect} from 'react-redux'
import store from './Redux/store'
import Root from './Components/Root'

import i18n from './i18n/i18n'


export default function App (props){
  return (
    <Provider store={store}>
      <Root/>
    </Provider>
  )
}
