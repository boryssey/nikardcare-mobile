import { AsyncStorage } from 'react-native';
import {connect} from 'react-redux'
const deviceStorage = {
  async saveItem(key, value) {
    try {

        await AsyncStorage.setItem(key, value);
    } catch (error) {
      console.log('AsyncStorage Error: ' + error.message);
    }
  },
  async loadTokens(){
    let res = {}
    return await AsyncStorage.multiGet(["token", "refreshToken"]).then(response => {
      return {'token': response[0][1], 'refreshToken':response[1][1]}
    });
  },
  async clearTokens(){
    try{
      await AsyncStorage.clear()
    } catch(error){
      console.log(error.message)
    }
  },
  async updateToken(newToken, newRefreshToken){
    try{
      await AsyncStorage.multiSet([['token', newToken], ['refreshToken', newRefreshToken]])
    } catch(error){
      console.log(error.message)
    }
  }
};

export default deviceStorage;
