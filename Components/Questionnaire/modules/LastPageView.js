import React, {useEffect} from 'react'
import {View, StyleSheet} from 'react-native';
import MyText from '../../Shared/MyText'

export default function FileView(props){

  useEffect(()=>{
    props.setIsEmpty(false)
  }, [])

  return(
    <View style={styles.container}>
      <MyText style={styles.headerLabel} variant = 'SemiBold'>Submit results?</MyText>
      <MyText style={styles.bodyLabel} variant = 'SemiBold'>You will not be able to change the answers after submitting</MyText>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    margin: 25,
    alignItems: 'center'
  },
  headerLabel: {
    fontSize: 40,
    marginTop: 50
  },
  bodyLabel: {
    marginTop: 50,
    fontSize: 30,
    margin: 15,
    textAlign: 'center'
  }

})
