import React, {useEffect, useState} from 'react'
import {View, StyleSheet, ActionSheetIOS, Platform, TouchableOpacity} from 'react-native';
import MyText from '../../Shared/MyText';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import Dimensions from 'Dimensions';
const {width, height} = Dimensions.get('window');
import { useActionSheet } from '@expo/react-native-action-sheet'
import * as DocumentPicker from 'expo-document-picker';
import {blue, lightgray} from '../../../utils/colors'
import {MaterialIcons} from '@expo/vector-icons'
import {Entypo} from '@expo/vector-icons'
import * as mime from 'react-native-mime-types'
import Dialog from "react-native-dialog";
import * as FileSystem from 'expo-file-system';
import {Ionicons} from '@expo/vector-icons'
import {connect} from 'react-redux'

var id = 0;
ID = () => {
  return '_' + Math.random().toString(36).substr(2, 9);
}

const getId = () => {
  return id++;
}

function FileView(props){
  const [files, setFiles] = useState([]);
  const { showActionSheetWithOptions } = useActionSheet();
  const [openDialog, setOpenDialog] = useState(false)
  const [curentFile, setCurentFile] = useState(undefined)

  useEffect(()=>{
    setFiles(props.answer.attachments)
    getPermissionAsync()
    props.setIsEmpty(false)
  }, [])

  useEffect(() => {
    props.updateAnswer(props.question.idQuestion, files)
  }, [files])
  addNewFile = (fileName) => {
    curentFile.fileName = fileName
    setFiles(files.concat([curentFile]))
  }

  deleteFileFromList = (id) => {
    setFiles(files.filter((file) => file.id !== id))
  };

  FileNameDialog = (props) =>{
    const [fileName, setFileName] = useState('')

    saveFile = () => {
      props.setOpen()
      addNewFile(fileName);
    }
    return(
      <View>
        <Dialog.Container visible = {props.openDialog}>
          <Dialog.Title>Input file name</Dialog.Title>
          <Dialog.Input style = {styles.fileInputAndroid} value = {fileName} onChangeText = {(text) => setFileName(text)}/>
          <Dialog.Button label="Cancel" onPress = {()=>{props.setOpen()}}/>
          <Dialog.Button label="Save" onPress = {()=>{saveFile()}}/>
        </Dialog.Container>
      </View>
    )
  }

  const _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      quality: 0.5,
      base64: true,
      exif:true
    });
    console.log(result)
    if (!result.cancelled) {
      var extension = result.uri.split('.').pop();
      const contentType = mime.lookup('.' + extension);
      const filename = result.uri.substring(result.uri.lastIndexOf("/") + 1, result.uri.lastIndexOf("."));
      const fileInfo = await FileSystem.getInfoAsync(result.uri)
      if(!fileInfo.exists){
        console.log('doesnt exist')
        return;
      }
      if(4 * Math.ceil((result.base64.length / 3) * 0.5624896334383812) > 8000000){
        alert('Size of the file is too big')
        return;
      }
      console.log(fileInfo.size)
      const fileN = props.user.UserName.split(' ').join('') + (new Date()).getMilliseconds() + '_' + getId();
      setFiles(files.concat([{fileContentBase64: result.base64, fileName: fileN, uri: result.uri, contentType, fileExtension: extension, id: ID()}]))
      // setOpenDialog(true);
      // setCurentFile({fileContentBase64: result.base64, uri: result.uri, contentType, fileExtension: extension, id: ID()})
    }
  };

  const _pickDocument = async () => {
    let result = await DocumentPicker.getDocumentAsync({
      type:'application/pdf',
      base64: true
    });
    if (result.type !== 'cancel') {
      var extension = result.uri.split('.').pop();
      const contentType = mime.lookup('.' + extension);
      const res = await FileSystem.getInfoAsync(result.uri)
      if(res.size > 8000000){
        alert('Size of the file is too big')
        return;

      }
      FileSystem.readAsStringAsync(result.uri, {encoding: FileSystem.EncodingType.Base64}).then((base64) => {
        setFiles(files.concat([{fileContentBase64: base64, uri: result.uri, contentType, fileName: result.name.split('.').slice(0, -1).join('.'), fileExtension: extension, id: ID()}]))
      })
    }

  }

  const _takePhoto = async () => {
    let result = await ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      quality: 0.5,
      base64: true,
    });
    if (!result.cancelled) {
      var extension = result.uri.split('.').pop();
      const contentType = mime.lookup('.' + extension);
      const filename = result.uri.substring(result.uri.lastIndexOf("/") + 1, result.uri.lastIndexOf("."));
      const fileInfo = await FileSystem.getInfoAsync(result.uri)
      if(!fileInfo.exists){
        return;
      }
      if(fileInfo.size > 8000000){
        alert('Size of the file is too big')
        return;
      }
      const fileN = props.user.UserName.split(' ').join('') + (new Date()).getMilliseconds() + '_' + (files.length+1);
      setFiles(files.concat([{fileContentBase64: result.base64, fileName: fileN, uri: result.uri, contentType, fileExtension: extension, id: ID()}]))
      // setCurentFile({fileContentBase64: result.base64, uri: result.uri, contentType, fileExtension: extension, id: ID()})
    }
  };
  const showDialog = () => {
    showActionSheetWithOptions(
      {
        options: ['Cancel', 'Take photo', 'Choose photo', 'Choose file'],
        cancelButtonIndex: 0,
      },
      (buttonIndex) => {
        switch (buttonIndex){
          case 1:
            _takePhoto();
            break;
          case 2:
            _pickImage();
            break;
          case 3:
            _pickDocument();
            break;
          default:
            break;
        }
      },
    );
  }
  const getPermissionAsync = async () => {

      let { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
      ({ status } = await Permissions.askAsync(Permissions.CAMERA));
      if (status !== 'granted') {
        alert('Sorry, we need camera permissions to make this work!');
      }

  }
  const checkPermission = async () => {
    const res = await Permissions.getAsync(Permissions.CAMERA);
  }

  return(
    <View style = {styles.container}>
      <MyText style = {styles.title}>
        {props.question.title}
      </MyText>
      <FileNameDialog openDialog = {openDialog} setOpen={() => setOpenDialog(false)}/>
      <View style = {styles.filesContainer}>
        {files.map((file, index) =>
            <View style = {styles.fileItem} key = {index}>
              <View style = {styles.fileNameContainer}>
                {file.contentType.includes('image')
                ? <MaterialIcons name = 'photo' style = {styles.imageItemIcon} size = {50}/>
                : <Ionicons name = 'ios-document' style = {styles.fileItemIcon} size = {50}/>}
                <MyText style = {styles.fileItemText}>{file.fileName}</MyText>
              </View>
              <Entypo name = 'cross' size ={30} color = 'red' onPress = {() => deleteFileFromList(file.id)}/>
            </View>
          )
        }
      </View>
      <View style = {styles.buttonsRow}>
        <TouchableOpacity style = {styles.uploadButton} onPress = {() => showDialog()}>
          <MyText style = {styles.buttonText}>
            Select file/photo
          </MyText>
        </TouchableOpacity>
      </View>
    </View>
  )
}
const mapStateToProps = state => ({
  user: state.user,
})

export default connect(mapStateToProps)(FileView)
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  filesContainer: {
    borderWidth: 1,
    margin: 25,
    height: 275,
    borderRadius: 10,
    borderColor: lightgray
  },
  buttonsRow: {
    display: 'flex',
    alignItems: 'flex-end'
  },
  uploadButton: {
    backgroundColor: blue,
    marginRight: 35,
    marginTop: 10
  },
  buttonText: {
    margin: 10,
    color: 'white',
    fontSize: 20
  },
  title: {
    textAlign: 'center',
    fontSize: 35
  },
  fileItem: {
    marginTop: 10,
    margin: 5,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  imageItemIcon:{
    color: blue,
  },
  fileItemIcon: {
    margin: 5,
    color: blue
  },
  fileItemText:{
    maxWidth: 225,
    fontSize: 15
  },
  fileNameContainer: {
    flex: 1,
    display:'flex',
    flexDirection:'row',
    alignItems: 'center'
  },
  fileInputAndroid: {
    borderBottomWidth: 1,
    borderColor: lightgray
  },
  blank: {

  }


});
