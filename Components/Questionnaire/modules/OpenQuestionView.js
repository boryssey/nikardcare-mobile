import React, {useState, useEffect} from 'react'
import {View, Text, StyleSheet, TextInput, ScrollView, KeyboardAvoidingView} from 'react-native'
import MyText from '../../Shared/MyText'
import {darkblue, lightgray, gray} from '../../../utils/colors'

export default function OpenQuestionView (props){
  const [currentLimit, setCurrentLimit] = useState(0)
  const [error, setError] = useState('')
  const [value, setText] = useState('')
  const {question} = props;

  onTextChange = (text) => {
    setText(text)
    props.updateAnswer(question.idQuestion, text)

  }
  useEffect(() => {
    setText(props.answer.text)
  }, [])

  useEffect(() => {
    setCurrentLimit(value.replace(/[\r\n]/g, '').length)
    if(value.replace(/[\r\n]/g, '').length > question.limit){
      setError('You reached the character limit')
      props.setIsEmpty(true)
    }else if(value !== ''){
      props.setIsEmpty(false)
      setError('')
    } else if(value === ''){
      props.setIsEmpty(true)
      setError('')
    }
  }, [value])


  return (
    <KeyboardAvoidingView style = {{flex: 1}} behavior="position" keyboardVerticalOffset = {30} enabled>
      <ScrollView contentContainerStyle = {styles.container}>
        <View style = {styles.titleContainer}>
          <MyText style = {styles.titleText} variant = 'SemiBold'>
            {question.title}
          </MyText>
        </View>
        <ScrollView contentContainerStyle = {styles.answerContainer}>
          <View style = {styles.limitContainer}>
            <MyText style = {styles.limitText}>
                {currentLimit}/{props.question.limit}
            </MyText>
          </View>
          <TextInput
            style = {styles.textInput}
            multiline = {true}
            value = {value}
            onChangeText = {text => onTextChange(text)}
            placeholder = 'Enter answer here'
            />
          <MyText style = {styles.errorText}>
            {error}
          </MyText>
        </ScrollView>
      </ScrollView>
    </KeyboardAvoidingView>
  )
}


const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    margin: 30
  },
  titleContainer: {
    alignItems: 'center',
  },
  titleText: {
    fontSize: 25,
    color: darkblue,
  },
  answerContainer: {
    marginTop: 50,
  },
  textInput: {
    fontSize: 15,
    paddingTop: 25,
    padding: 15,
    textAlignVertical: 'top',
    color: darkblue,
    borderWidth: 1,
    borderColor: lightgray,
    borderRadius: 15,
    justifyContent: 'flex-start',
    height: 275
  },
  limitText:{
    color: gray,
    alignSelf: 'flex-end',
    marginRight: 12
  },
  errorText :{
    color: 'red',
    marginLeft: 12
  }
});
