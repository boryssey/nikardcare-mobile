import React, {useState, useEffect} from 'react'
import {View, Text, StyleSheet, TextInput, KeyboardAvoidingView, ScrollView} from 'react-native'
import MyText from '../../Shared/MyText'
import {darkblue, lightgray, gray} from '../../../utils/colors'
function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}
export default function ValueChoiceView (props){
  const {question} = props;
  const [value, setValue] = useState('');
  const [isError, setError] = useState(false);
  const [errorText, setErrorText] = useState('')
  const number = /^\d*.{1}\d+$|\d+$/

  onChangeText = (text) => {
    setValue(text)
  }
  useEffect(() => {
    if(props.answer.value !== -0){
      onChangeText(props.answer.value.toString())
      props.setIsEmpty(false)
    }
  }, [])

  useEffect(() => {
    if(value.includes(',')){
      setValue(value.replace(/,/g, '.'))
    }
    if(value === ''){
      props.setIsEmpty(true)
      setError(false)
      setErrorText('')
    } else if(isNaN(Number(value))  && value !== ''){
    // if(true && value !== ''){
      setError(true)
      setErrorText('Must be a number')
      props.setIsEmpty(true)
    } else if(value !== ''){
      props.updateAnswer(question.idQuestion, Number(value))
      props.setIsEmpty(false)
      setError(false)
      setErrorText('')
    }

  }, [value])


  return (
    <KeyboardAvoidingView style = {{flex: 1}}>
      <ScrollView contentContainerStyle = {styles.container}>
        <View style = {styles.titleContainer}>
          <MyText style = {styles.title} variant = 'SemiBold'>
            {question.title}
          </MyText>
        </View>
        <View style = {styles.answerContainer}>
          <MyText style = {styles.textFieldLabel}>
            Enter your answer here
          </MyText>
          <TextInput
            style = {styles.inputField}
            onChangeText = {text => onChangeText(text)}
            value = {value}
            keyboardType = 'numeric'
          />
        <MyText style = {styles.errorText}>
            {errorText}
          </MyText>
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
  title: {
    color: darkblue,
    fontSize: 25,
    textAlign: 'center',
  },
  titleContainer: {
    textAlign: 'center',
    marginTop: 30
  },
  answerContainer: {
    flex: 1,
    margin: 70,
    marginTop: 100,
  },
  inputField:{
    fontFamily:'Source Sans Pro',
    color: darkblue,
    fontSize: 25,
    height:60,
    borderWidth: 1,
    borderColor: lightgray,
    padding: 5,
    minWidth: 100
  },
  textFieldLabel: {
    color: gray,
    fontSize: 25,
    marginBottom: 20
  },
  errorText: {
    color: 'red'
  },
  boundariesText: {
    fontSize: 15,
    color: 'gray'

  }
});
