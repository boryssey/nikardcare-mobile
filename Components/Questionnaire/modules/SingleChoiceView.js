import React, {useState, useEffect} from 'react'
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native'
import MyText from '../../Shared/MyText'
import {Ionicons} from '@expo/vector-icons'
import {darkblue} from '../../../utils/colors'


function ChoiceOption(props){
  const {choiceOption} = props;
  return(
    <TouchableOpacity style = {styles.answerOptionContainer} onPress = {selectHandler(choiceOption.idChoiceOption)}>
        {
          choiceOption.idChoiceOption === props.selectedIndex ?
          <Ionicons name = 'ios-radio-button-on' color={darkblue} size={25} />
          : <Ionicons name = 'ios-radio-button-off' color={darkblue} size={25}/>
        }
      <MyText style={styles.answerText}>
        {choiceOption.value}
      </MyText>
    </TouchableOpacity>
  )
}

export default function SingeChoiceView (props){
  const {question} = props;
  const [selectedIndex, setSelectedIndex] = useState(-1);
  selectHandler = (index) => () => {
    setSelectedIndex(index)
  }
  useEffect(() => {
    console.log(selectedIndex)
    setSelectedIndex(props.answer.choiceOptionIds.length !== 0
      ? props.answer.choiceOptionIds[0]
      : -1)
  }, [])

  useEffect(() => {
    props.updateAnswer(question.idQuestion, selectedIndex);
    if(selectedIndex !== -1){
      props.setIsEmpty(false)
    } else{
      props.setIsEmpty(true)
    }

  }, [selectedIndex])


  return (
    <View style={styles.container}>
      <View style = {styles.titleContainer}>
        <MyText style = {styles.title} variant='SemiBold'>
          {question.title}
        </MyText>
        <MyText style = {styles.semiTitle} variant='SemiBold'>
          (Choose one option)
        </MyText>
      </View>
      <View style={styles.answersContainer}>

        {question.choiceOptions.map((choiceOption, index) => {
          return(
            <ChoiceOption
              choiceOption = {choiceOption}
              key = {index}
              selectedIndex = {selectedIndex}
              selectHandler={selectHandler}/>
          )
        })}
      </View>
    </View>
  )
}


const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    margin: 25,
    flex: 1,
  },
  titleContainer: {
    alignSelf: 'center',
    flexDirection: 'column',
    alignItems: 'center',

  },
  title: {
    color: darkblue,
    fontSize: 25
  },
  semiTitle: {
    color: darkblue,
    fontSize: 20
  },
  answersContainer: {
    flex: 1,
    flexDirection: 'column',
    alignSelf: 'flex-start',
    marginTop: 50,
    marginLeft: 25
  },
  answerOptionContainer: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 5
  },
  answerText: {
    color: darkblue,
    fontSize: 25,
    marginLeft: 10
  }
});
