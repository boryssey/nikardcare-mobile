import React, {useState, useEffect} from 'react'
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native'
import MyText from '../../Shared/MyText'
import {MaterialCommunityIcons} from '@expo/vector-icons'
import {darkblue} from '../../../utils/colors'

function ChoiceOption(props){
  const {choiceOption} = props;
  return(
    <TouchableOpacity style = {styles.answerOptionContainer} onPress = {selectHandler(choiceOption.idChoiceOption)}>
        {
          isSelected(choiceOption.idChoiceOption) ?
          <MaterialCommunityIcons name = 'checkbox-intermediate' color = {darkblue} size={25} />
          : <MaterialCommunityIcons name = 'checkbox-blank-outline' color = {darkblue} size={25}/>
        }
      <MyText style={styles.answerText}>
        {choiceOption.value}
      </MyText>
    </TouchableOpacity>
  )
}



export default function MultipleChoiceView (props){
  const {question, selectedIndex, selectedHandler} = props;
  const [selectedAnswers, setSelectedAnswers] = useState([]);

  selectHandler = (index) => () => {
    if(selectedAnswers.includes(index)){
      setSelectedAnswers(selectedAnswers.filter(el => el != index));
    } else{
      selectedAnswers.push(index)
      setSelectedAnswers([...selectedAnswers])
    }

  }
  isSelected = (index) => {
    return selectedAnswers.includes(index);
  }

  useEffect(() => {
    setSelectedAnswers(props.answer.choiceOptionIds)
  }, [])

  useEffect(() => {
    props.updateAnswer(question.idQuestion, selectedAnswers);
    if(selectedAnswers.length !== 0){
      props.setIsEmpty(false)
    } else{
      props.setIsEmpty(true)
    }

  }, [selectedAnswers])

  return (
    <View style={styles.container}>
      <View style = {styles.titleContainer}>
        <MyText style = {styles.title} variant='SemiBold'>
          {question.title}
        </MyText>
        <MyText style = {styles.semiTitle} variant='SemiBold'>
          (Choose one or more options)
        </MyText>
      </View>
      <View style={styles.answersContainer}>
        {question.choiceOptions.map((choiceOption, index) => {
          return(
            <ChoiceOption
              choiceOption = {choiceOption}
              key = {index}
              id = {index}
              isSelected = {isSelected}
              selectedIndex = {selectedIndex}
              selectHandler={selectHandler}/>
          )
        })}
      </View>
    </View>
  )
}


const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    margin: 25,
    flex: 1,
  },
  titleContainer: {
    alignSelf: 'center',
    flexDirection: 'column',
    alignItems: 'center',
  },
  title: {
    color: darkblue,
    fontSize: 25
  },
  semiTitle: {
    color: darkblue,
    fontSize: 20
  },
  answersContainer: {
    flex: 1,
    flexDirection: 'column',
    alignSelf: 'flex-start',
    marginTop: 50,
    marginLeft: 25
  },
  answerOptionContainer: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 5
  },
  answerText: {
    color: darkblue,
    fontSize: 25,
    marginLeft: 10
  }
});
