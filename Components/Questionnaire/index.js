import React, {useState, useEffect} from 'react';
import {ActivityIndicator, Platform, View, Text, StyleSheet, Button, TouchableOpacity, BackHandler} from 'react-native';
import { useHistory } from "react-router-dom";
import MyText from '../Shared/MyText'
import {white, black} from '../../utils/colors'
import {Ionicons} from '@expo/vector-icons'
import SingleChoiceView from './modules/SingleChoiceView'
import FileView from './modules/FileView'
import MultiplChoiceView from './modules/MultipleChoiceView'
import ValueChoice from './modules/ValueChoiceView'
import OpenQuestionView from './modules/OpenQuestionView'
import LastPageView from './modules/LastPageView'
import {red, darkblue} from '../../utils/colors'
import {getQuestionnaire, postQuestionnaire} from '../../api/QuestionnaireAPI'
import {useParams, useRouteMatch} from 'react-router-dom'
import {connect} from 'react-redux'

var id = 0;
ID = () => {
  id += 1;
  return id;
}


function QuestionnaireIndex(props){
  const [answers, setAnswers] = useState([]);
  const [currentIndex, setCurrentIndex] = useState(0);
  const [isEmpty, setIsEmpty] = useState(true)
  const [isLoading, setLoading] = useState(true);
  const [questionnaire, setQuestionnaire] = useState(undefined)


  let history = useHistory();

  useEffect(() => {
    getQuestionnaire(props.user.id, props.match.params.id).then((resp) => {
        setQuestionnaire(resp.data)
    }).catch((err) => {
      console.log(err.response)
    })
    BackHandler.addEventListener('hardwareBackPress', backButtonHandler);
  }, [])
  useEffect(() => {
    let newArr = []
    if(questionnaire){
      questionnaire.questions.forEach((question) => {
        switch(question.questionType){
          case("Single choice"):
              newArr.push({
                idQuestion: question.idQuestion,
                idQuestionType: 1,
                choiceOptionIds: [-1],
                value: -0,
                text:'',
                attachments: []
              })
              break;
          case("Multiple choice"):
            newArr.push({
              idQuestion: question.idQuestion,
              idQuestionType: 2,
              choiceOptionIds: [
              ],
              text:'',
              value: -0,
              attachments: []
            })
            break;
          case("Value"):
            newArr.push({
              idQuestion: question.idQuestion,
              idQuestionType: 3,
              choiceOptionIds: [
              ],
              text: '',
              value: -0,
              attachments: []
            })
            break;
          case("Open"):
            newArr.push({
              idQuestion: question.idQuestion,
              idQuestionType: 4,
              choiceOptionIds: [
              ],
              value: -0,
              text: '',
              attachments: []
            })
            break;
          case("File"):
            newArr.push({
              idQuestion: question.idQuestion,
              idQuestionType: 5,
              choiceOptionIds: [
              ],
              text:'',
              value: -0,
              attachments: []
            })
            break
          default:
            throw new Error();
        }
      })
      setAnswers(newArr)
      setLoading(false)
    }
  }, [questionnaire]);


  updateOpenQuestion = (id, answer) => {
    let foundEl = answers.find(el => el.idQuestion === id);
    foundEl.text = answer;
    setAnswers([...answers])
  }

  updateSingleChoice = (id, answer) =>{
    if(answers.length === 0){
      return 0;
    };
    let foundEl = answers.find(el => el.idQuestion === id);
    foundEl.choiceOptionIds[0] = answer;
    setAnswers([...answers])
  }
  updateMultipleChoice = (id, newAnswers) => {
    let foundEl = answers.find(el => el.idQuestion === id);
    foundEl.choiceOptionIds = newAnswers;
    setAnswers([...answers])
  }

  updateValueAnswer = (id, answer) => {
    let foundEl = answers.find(el => el.idQuestion === id);
    foundEl.value = answer;
    setAnswers([...answers])
  }
  updateFileAnswer = (id, answer) => {
    let newAnswer = answer;
    newAnswer.map((a) =>{ delete a.uri;})
    let foundEl = answers.find(el => el.idQuestion === id);
    foundEl.attachments = newAnswer;
    setAnswers([...answers])
  }

  getAnswerById = (id) => {

    if(answers === []){
      return 0;
    };
    return answers.find(el => el.idQuestion === id);
  }

  getView = (question) => {
    switch(question.questionType){
      case "Single choice":
        return <SingleChoiceView
                  setIsEmpty={setIsEmpty}
                  question = {question}
                  answer = {getAnswerById(question.idQuestion)}
                  updateAnswer = {updateSingleChoice}/>;
      case "Multiple choice":
        return <MultiplChoiceView
          setIsEmpty={setIsEmpty}
          question = {question}
          answer = {getAnswerById(question.idQuestion)}
          updateAnswer = {updateMultipleChoice}/>;
        case "Open":
        return <OpenQuestionView
          setIsEmpty={setIsEmpty}
          question = {question}
          answer = {getAnswerById(question.idQuestion)}
          updateAnswer = {updateOpenQuestion}/>;
      case "Value":
        return <ValueChoice setIsEmpty={setIsEmpty}
        question = {question}
        answer = {getAnswerById(question.idQuestion)}
        updateAnswer = {updateValueAnswer}/>;
      case "File":
        return <FileView
          setIsEmpty = {setIsEmpty}
          question = {question}
          nextHandler = {nextHandler}
          answer = {getAnswerById(question.idQuestion)}
          updateAnswer = {updateFileAnswer}/>;
      default:
        return;
    }



  }

  isLast = () => {
    return (questionnaire.questions.length === currentIndex)
  }
  isFirst = () =>{
    return (currentIndex === 0)
  }
  handleFinish = () => {
    postQuestionnaire(props.match.params.id, {idQuestionnaireAppointment: props.match.params.id,answers}).then((resp) => {
      history.push('/')
    }).catch((err) => {
      console.log(err.response)
    })
  }
  nextHandler = () => {
    if(!isLast()){
      setCurrentIndex(currentIndex + 1)
    }
  }
  backHandler = () => {
    if(currentIndex !== 0){
      setCurrentIndex(currentIndex - 1)
    }
  }
  backToCalendarBtn = () => {
    history.push('/')
  }
  backButtonHandler = () => {
    if(isFirst()){
      backToCalendarBtn()
    }else{
      backHandler()
    }
  }
  if(isLoading) {
    return (
      <View style={{flex:1, display:'flex', alignItems:'center', justifyContent:'center'}}>
        <ActivityIndicator size='large' color={red} />
      </View>
    )
  }
  return(
    <View style={styles.container}>
      <TouchableOpacity
        style={isFirst() ? styles.backToCalendarBtn : [styles.backToCalendarBtnHidden, styles.backToCalendarBtn]}
        disabled = {!isFirst()}
        onPress = {backToCalendarBtn}>
        <Ionicons name='ios-arrow-back' color={darkblue} size={30}/>
        <MyText style={styles.navigationButtonText}>
          Calendar
        </MyText>
      </TouchableOpacity>
      <View style={styles.questionContainer}>
        {isLast() ? <LastPageView setIsEmpty = {setIsEmpty}/>: getView(questionnaire.questions[currentIndex])}

      </View>
      <View style={styles.navigationContainer}>
        <TouchableOpacity
          style={isFirst() ? styles.navigationButtonHidden : styles.navigationButton}
          disabled = {isFirst()}
          onPress = {backHandler}
          >
          <Ionicons name='ios-arrow-back' color={darkblue} size={30}/>
          <MyText style={styles.navigationButtonText}>
            Back
          </MyText>
        </TouchableOpacity>
        {isLast()
          ? (<TouchableOpacity
            style={ isEmpty ? styles.navigationButtonHidden : styles.navigationButton }
            disabled={isEmpty}
            onPress={() => {handleFinish()}}
            >
            <MyText style={styles.navigationButtonText}>
              Finish
            </MyText>
            <Ionicons name='ios-arrow-forward' color={darkblue} size={30}/>
          </TouchableOpacity>)
          : (<TouchableOpacity
          style={ isEmpty ? styles.navigationButtonHidden : styles.navigationButton }
          disabled={isEmpty}
          onPress={nextHandler}
          >
          <MyText style={styles.navigationButtonText}>
            Next
          </MyText>
          <Ionicons name='ios-arrow-forward' color={darkblue} size={30}/>
        </TouchableOpacity>)}
      </View>
    </View>
  )
}
const mapStateToProps = state => ({
  user_tokens: state.user_tokens,
  user: state.user,
})

export default connect(mapStateToProps)(QuestionnaireIndex)

const styles = StyleSheet.create({
  container: {
    flex:1,
  },
  questionContainer: {
    flex:1,
  },
  navigationContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 40,
    marginRight: 25,
    marginLeft: 25
  },
  navigationButton: {
    flexDirection: 'row',
    backgroundColor: white,
    borderWidth: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  navigationButtonHidden: {
    flexDirection: 'row',
    backgroundColor: white,
    borderWidth: 0,
    justifyContent: 'center',
    alignItems: 'center',
    opacity:0
  },
  navigationButtonText: {
    color: darkblue,
    fontSize: 25,
    marginLeft: 5,
    marginRight: 10
  },
  backToCalendarBtnHidden:{
    opacity:0
  },
  backToCalendarBtn: {
    alignSelf: 'flex-start',
    marginTop:40,
    marginLeft: 25,
    flexDirection:'row',
    justifyContent: 'center',
    alignItems: 'center'
  }
});
