import React from 'react'
import {connect} from 'react-redux'
import { Route, Redirect } from "react-router-native";

const PrivateRoute = ({ component: Component,isAuth, ...rest }) => (
  <Route {...rest} render={(props) => (
    isAuth
      ? <Component {...props} />
      : <Redirect to='/login' />
  )} />
)
const mapStateToProps = state => ({
  isAuth: state.isAuth,
})

export default connect(mapStateToProps)(PrivateRoute)
