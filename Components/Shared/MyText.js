import React from 'react';
import {View, Text, StyleSheet, } from 'react-native';


export default function MyText(props){
  const {style, children, variant ,...rest} = props;

  const getStyle = () => {
    switch(variant){
      case 'Bold':
        return FontStyle.Bold
      case 'SemiBold':
        return FontStyle.SemiBold
      default:
        return FontStyle.regular
    }
  }

  return(
    <View>
      <Text style={[getStyle(), style]} {...rest}>{children}</Text>
    </View>
  )
}


const FontStyle = StyleSheet.create({
  regular:{
    fontFamily:'Source Sans Pro',
  },
  Bold:{
    fontFamily:'Source Sans Pro Bold',
  },
  SemiBold:{
    fontFamily:'Source Sans Pro SemiBold',
  }
});
