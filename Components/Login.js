import React, {useState, useEffect} from 'react'
import { useHistory } from "react-router-dom";
import {View, KeyboardAvoidingView, Image, StyleSheet, TextInput, TouchableOpacity, Keyboard} from 'react-native'
import MyText from './Shared/MyText'
import {blue, white, darkblue} from '../utils/colors'
import {loginUser, loginError} from '../Redux/Actions/AuthActions'
import {login} from '../api/AuthAPI'
import deviceStorage from '../utils/deviceStorage'
import { useTranslation } from 'react-i18next';
import {connect} from 'react-redux'

function Login (props){
  const [emailValue, setEmailValue] = useState('');
  const [passwordValue, setPasswordValue] = useState('')
  const [tokens, setTokens] = useState({})
  const {t, i18n} = useTranslation()


  let history = useHistory();

  const handleLoginButton = () => {
    login(emailValue, passwordValue)
    .then((response)=> {
      console.log(response)
      if(response.status === 200){
          props.loginUser(response.data)
          deviceStorage.saveItem("token", response.data.token);
          deviceStorage.saveItem("refreshToken", response.data.refreshToken)
      }
    }).catch((a) => {
      if(a.response.status === 400){
        props.loginError('Wrong email or password')
      }

    })
  }
  useEffect(() => {
    if(props.isAuth){
      if(props.user.role !== "Patient"){
        props.loginError("Doctor cannot sign in to the patient's application")
        deviceStorage.clearTokens();
      } else{
        history.push('/')
      }
    }
  }, [props.isAuth])
  return (
    <KeyboardAvoidingView contentContainerStyle={styles.container} behavior = 'position'  onPress = {() => {Keyboard.dismiss()}}>
      <Image style = {styles.logoImage} source={require('../Images/Logo.png')}/>
      <MyText style = {styles.LogoText} variant = 'SemiBold'>
        NikardCare
      </MyText>
      <TextInput
        style = {styles.input}
        value = {emailValue}
        onChangeText = {text => setEmailValue(text)}

        placeholder = 'Email'
        autoCompleteType = 'email'
        keyboardType = 'email-address'
      />
      <TextInput
        style = {styles.input}
        value = {passwordValue}
        onChangeText = {text => setPasswordValue(text)}
        placeholder = 'Password'
        secureTextEntry = {true}
      />
    {props.error !== null ? (
      <MyText style = {styles.errorText}>
      {  props.error}
      </MyText>
    ) : null}
      <TouchableOpacity style = {styles.loginButton} onPress={handleLoginButton}>
          <MyText style = {styles.loginButtonText}>
            {t('login')}
          </MyText>
        </TouchableOpacity>
    </KeyboardAvoidingView>
  )
}
const mapStateToProps = state => ({
  isAuth: state.isAuth,
  user: state.user,
  error: state.error
})

const mapDispatchToProps = dispatch => {
  return {
    loginUser: (user) => dispatch(loginUser(user)),
    loginError: (error) => dispatch(loginError(error))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Login)

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  logoImage: {
    width: 250,
    height: 250
  },
  LogoText: {
    fontSize: 30,
    color: darkblue
  },
  input: {
    marginTop: 20,
    borderBottomWidth: 2,
    borderColor: '#E9E9F0',
    width: 300,
    height: 50,
    fontSize: 20,
    color: darkblue
  },
  loginButtonText: {
    color: white,
    marginTop: 12,
    marginBottom: 12,
    marginLeft: 50,
    marginRight: 50,
    fontSize: 25
  },
  loginButton: {
    backgroundColor: blue,
    margin: 15,
    marginTop: 50,
    borderRadius: 20
  },
  errorText: {
    color: 'red'
  }
});
