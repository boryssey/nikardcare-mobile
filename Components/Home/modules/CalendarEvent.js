import React, {useState} from 'react'
import {View, Text, StyleSheet, TouchableOpacity, ScrollView} from 'react-native'
import { useHistory } from "react-router-dom";
import MyText from '../../Shared/MyText'
import {lightgray, gray, red, white, blue, pink, purple, green} from '../../../utils/colors'
import moment from 'moment'

function QuestionnaireEvent(props) {
  let history = useHistory();

  const getStyle = (status) => {
    switch(status){
      case 1:
        return styles.availableQuestionnaire
      case 2:
        return styles.notAvailableQuestionnaire
      case 3:
        return styles.answeredQuestionnaire
      case 4:
        return styles.missedQuestionnaire
      default:
        return styles.QuestionnaireEvent
    }
  }
  const getStatus = (status) => {
    switch(status){
      case 1:
        return 'Availalbe'
      case 2:
        return 'Not available'
      case 3:
        return 'Finished'
      case 4:
        return 'Missed'
      default:
        return 'Not available'
    }
  }
  const getAction = (status) => {
    switch(status){
      case 1:
        history.push('/Questionnaire/' + props.event.idEvent)
        break
      case 2:
        alert('This questionnaire is not yet available')
        break
      case 3:
        alert('You already finished this questionnaire')
        break
      case 4:
        alert('You cannot answer this questionnaire anymore')
        break
      default:
        alert('Something went wrong')

    }
  }
  return (
    <TouchableOpacity style={[styles.EventContainer, styles.QuestionnaireEvent, getStyle(props.event.questionnaireStatus)]} onPress={() => {getAction(props.event.questionnaireStatus)}}>
        <MyText style={styles.EventTitle}>
          Questionnaire
        </MyText>
        <MyText style={styles.Description}>
          {props.event.eventName + ' ('+ getStatus(props.event.questionnaireStatus) + ')'}
        </MyText>
    </TouchableOpacity>
  )
}


function VisitEvent(props){
  const [isOpen, setIsOpen] = useState(false);
  const {event} = props

  const onPressHandler = () => {
    setIsOpen(!isOpen);
  }

  return(
    <TouchableOpacity style={[styles.EventContainer, styles.VisitEvent]} onPress={onPressHandler}>
      <MyText style={styles.EventTitle}>
        Visit
      </MyText>
      <MyText style={styles.Description}>
        {moment(event.timeFrom).format('HH:mm')}
      </MyText>
      {
        isOpen
          ? (<View style={{
              display: 'flex',
              flexDirection: 'column'
            }}>
            <MyText style={styles.Description}>
              {event.description}
            </MyText>

          </View>)
          : null
      }
    </TouchableOpacity>
  )
}

export default function CalendarEvent(props) {
  const {event} = props;


  return (
    <View style={styles.container}>
      {!props.first ? <View style = {styles.DateContainer}></View> : <View style={styles.DateContainer}>
        <View style={styles.date}>
          <MyText style={styles.DateNumber}>
            { event.eventDate.format('DD') }
          </MyText>
          <MyText style={styles.DateText}>
            { event.eventDate.format('ddd') }
          </MyText>
        </View>
      </View>}
      {event.eventType === 'Visit' ? <VisitEvent event={event}/> : null}
      {event.eventType === 'Questionnaire' ? <QuestionnaireEvent event={event}/> : null}

    </View>)
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row'
  },
  Description: {
    color: white,
    fontSize: 15
  },
  EventContainer: {
    marginTop: 15,
    marginRight: 30,
    marginLeft: 20,
    width: 'auto',
    borderColor: lightgray,
    borderWidth: 1,
    borderRadius: 15,
    flex: 1,
    padding: 10,

  },
  QuestionnaireEvent: {
    backgroundColor:red
  },
  VisitEvent: {
    backgroundColor:blue
  },
  EventTitle: {
    color: white,
    fontSize: 20
  },
  date: {
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 10,
    marginBottom: 10,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  DateContainer: {
    width: 100,
    borderRightWidth: 1,
    borderColor: lightgray,
    paddingTop: 15
  },
  availableQuestionnaire: {
    backgroundColor: green
  },
  notAvailableQuestionnaire: {
    backgroundColor: red
  },
  answeredQuestionnaire: {
    backgroundColor: purple
  },
  missedQuestionnaire: {
    backgroundColor: lightgray
  },
  DateNumber: {
    fontSize: 30,
    color: gray,
    margin: -5
  },
  DateText: {
    fontSize: 20,
    color: gray,
    margin: -5
  }

});
