import React, {useState, useEffect} from 'react'
import {ActivityIndicator, View, Text, StyleSheet, FlatList, ScrollView, TouchableWithoutFeedback, RefreshControl, TouchableOpacity} from 'react-native'
import MyText from '../../Shared/MyText'
import CalendarEvent from './CalendarEvent'
import {connect} from 'react-redux'
import {gray, lightgray,white, red, blue} from '../../../utils/colors'
import moment from 'moment'
import {getCalendar} from '../../../api/CalendarAPI'
import {refresh} from '../../../api/AuthAPI'
import {refreshToken} from '../../../Redux/Actions/AuthActions'
import deviceStorage from '../../../utils/deviceStorage'
import {groupBy} from 'underscore'

function processInput(data){
  if(data.Visit){
    let result = data.Visit.concat(data.Examination, data.Questionnaire)
    result.forEach( event => event.eventDate = moment(event.eventDate))
    const groupedByDate = groupBy(result, (event) => {
      return event.eventDate.format('D')
    })

    return Object.keys(groupedByDate).map(function(key) {
      return [Number(key), groupedByDate[key]];
    });
  }
}

function Calendar(props) {
  const [apiData, setApiData] = useState({});
  const [isLoading, setLoading] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [selectedDate, setSelectedDate] = useState(moment())

  const getApiData = () => {
    getCalendar(props.user.id, selectedDate.year(), selectedDate.month()+1)
    .then((response) => {
      setApiData(response.data)
      setRefreshing(false)
      setLoading(false)
    })
    .catch((error) => {
      console.log(error.response)
      setRefreshing(false)
      setLoading(false)
    })
  }
  const onNextMonth = () => {
    setSelectedDate(selectedDate.clone().add(1, 'month'))
  }
  const onPrevMonth = () => {
    setSelectedDate(selectedDate.clone().subtract(1, 'month'))
  }
  const onCurrentMonth = () => {
    setSelectedDate(moment())
  }
  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    getApiData()
  }, [refreshing]);

  useEffect(() => {
    setLoading(true)
    getApiData()
  }, []);

  useEffect(() => {
    setLoading(true)
    getApiData()
  }, [selectedDate])

  const Loader = () => {
    return(
      <View style={{flex:1, display:'flex', alignItems:'center', justifyContent:'center'}}>
        <ActivityIndicator size='large' color={blue} />
      </View>
    )
  }
  const EmptyList = () => {
    return(
      <View style = {styles.emptyContainer}>
        <MyText style = {styles.emptyText} variant = 'SemiBold'>
          Empty
        </MyText>
      </View>
    )
  }
  const renderByDate = (date, ind) => {
    return(
      <React.Fragment>
        {date[1].map((event, index) => {
          return(
            <CalendarEvent event={event} first = {index === 0} key = {index.toString()}/>
          )
        })}
      </React.Fragment>
    )
  }
  return (
    <View style={styles.container}>
      <View style = {styles.calendarControlContainer}>
        <MyText style = {styles.monthTitle}>
          {selectedDate.format('MMMM YYYY')}
        </MyText>
        <View style = {styles.calendarButtonsContainer}>
          <TouchableOpacity style = {[styles.prevButton, styles.navButton, styles.notSelectedButton]} onPress = {onPrevMonth}>
            <MyText>
              Prev
            </MyText>
          </TouchableOpacity>
          <TouchableOpacity style = {[styles.curButton, styles.navButton, true ? styles.selectedButton : styles.notSelectedButton]} onPress = {onCurrentMonth}>
            <MyText style = {styles.navButtonText}>
              Current
            </MyText>
          </TouchableOpacity>
          <TouchableOpacity style = {[styles.nextButton, styles.navButton, styles.notSelectedButton]} onPress = {onNextMonth}>
            <MyText>
              Next
            </MyText>
          </TouchableOpacity>
        </View>
      </View>
      {isLoading ? <Loader/> : (<>
      <View style={styles.header}>
        <View style={styles.dateHeaderContainer}>
          <MyText style={styles.dateHeader} variant='SemiBold'>
            DATE
          </MyText>
        </View>
        <View style={styles.eventsHeaderContainer}>
          <MyText style={styles.eventsHeader} variant='SemiBold'>
            EVENTS
          </MyText>
        </View>
      </View>
      <View style={styles.ListContainer}>
        <FlatList
          contentContainerStyle={styles.list}
          data={processInput(apiData)}
          renderItem={({item, index}) => renderByDate(item, index)}
          ListEmptyComponent = {EmptyList}
          keyExtractor={index => index.toString()}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
        />
    </View></>)}
    </View>
  )
}
const mapStateToProps = state => ({
  user_tokens: state.user_tokens,
  user: state.user,
})
export default connect(mapStateToProps)(Calendar)

const styles = StyleSheet.create({
  container:{
    flex:1
  },
  ListContainer:{
    flex:1
  },
  eventsHeaderContainer: {
    flex: 1
  },
  dateHeader: {
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 10,
    marginBottom: 10,
    fontSize: 25,
    color: gray
  },
  eventsHeader: {
    marginLeft: 25,
    marginRight: 'auto',
    marginTop: 10,
    marginBottom: 10,
    fontSize: 25,
    color: gray
  },
  header: {
    display: 'flex',
    flexDirection: 'row'
  },
  dateHeaderContainer: {
    width: 100,
    borderRightWidth: 1,
    borderColor: white,
  },
  emptyContainer:{
    flexDirection: 'row',
    justifyContent: 'center',
    margin: 25,
  },
  emptyText:{
    fontSize: 25,
    color: gray
  },
  calendarControlContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  calendarButtonsContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems:'center',
    margin: 5
  },
  monthTitle: {
    color: gray,
    fontSize: 25,
    margin: 5,
    marginLeft: 10
  },
  navButton: {
    color: 'white',
    backgroundColor: 'white',
    borderWidth: 1,
    borderRadius: 5,
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 10,
    paddingRight: 10,
    margin: 2
  },
  navButtonText: {
  },
  selectedButton: {
    borderColor: blue,
  },
  notSelectedButton: {
    borderColor: lightgray,
  }

});
