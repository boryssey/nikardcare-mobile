import React from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux'
import {logoutUser} from '../../../Redux/Actions/AuthActions'
import UserAvatar from '../../../Icons/UserAvatar'
import MyText from '../../Shared/MyText';
import {gray} from '../../../utils/colors'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import deviceStorage from '../../../utils/deviceStorage'

function Header(props) {

  onLogout = () => {
    props.logoutUser()
    deviceStorage.clearTokens();
  }

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.userAvatar}>
        <UserAvatar height={60} width={60}/>
      </TouchableOpacity>
      <View style={styles.HeaderText}>
        <MyText style={styles.text}>
          Welcome
        </MyText>
        <TouchableOpacity>
          <MyText style={styles.SecondText}>
            {props.user.UserName}
          </MyText>
        </TouchableOpacity>
      </View>

      <TouchableOpacity>
        <MaterialCommunityIcons name='logout-variant' onPress={() => onLogout()} size={30} style={styles.logoutIcon}/>
      </TouchableOpacity>

    </View>)
}
const mapStateToProps = state => ({
  isAuth: state.isAuth,
  user: state.user
})

const mapDispatchToProps = dispatch => {
  return {
    logoutUser: () => dispatch(logoutUser())
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Header);
const styles = StyleSheet.create({
  container: {
    marginTop: 35,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  },
  userAvatar: {
    margin: 5,
    marginLeft:15
  },
  logoutIcon:{
    margin:10,
    marginRight:15,
    color:gray
  },
  HeaderText: {
    display: 'flex',
    flexDirection: 'column',
    margin:5,
    flex:1
  },
  text: {
    fontSize: 20
  },
  SecondText: {
    fontSize: 15,
    color:gray
  }
});
