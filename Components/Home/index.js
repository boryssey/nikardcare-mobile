import React, { useEffect,useState }  from 'react';
import { StyleSheet, Text, View, StatusBar,ScrollView, PanResponder, Button } from 'react-native';
import Calendar from './modules/Calendar'
import Header from './modules/Header'
import * as Font from 'expo-font';
import {connect} from 'react-redux'
import { AppLoading, Notifications } from 'expo';
import MyText from '../Shared/MyText'
import { Link } from "react-router-native";
import {pushToken} from '../../api/AuthAPI'
import {postToken} from '../../api/NotificationsApi'
import * as Permissions from 'expo-permissions';
import util from 'util'

function Home(props) {
  const [multiTouch, setMultitouch] = useState(false);
  const [error, setError] = useState([])
  useEffect(() => {
    registerForNotifications();
  }, [])
  const registerForNotifications = async () => {
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    setError(error.concat([status]))
    if (status !== 'granted') {
     alert('No notification permissions!');
     return;
    }
    try{
      let token = await Notifications.getExpoPushTokenAsync();
      console.log(token)
      postToken(props.user.email, token).then((res) => {
        console.log(res);
        setError(error.concat([res]))
      }).catch((err) => {
        console.log(err.response)
        setError(error.concat([err]))

      })
      pushToken(props.user.id, token).then((res) => {
        console.log(res);
        setError(error.concat([res]))
      }).catch((err) => {
        console.log(err.response)
        setError(error.concat([err]))

      })
    }catch(erro){
      console.log(erro.response)
    }
  }
  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <Header debuggerPanelHandler={setMultitouch}/>
      <Calendar />
    </View>
  )
}
const mapStateToProps = state => ({
  user_tokens: state.user_tokens,
  user: state.user,
})
export default connect(mapStateToProps)(Home)

const styles = StyleSheet.create({
  container: {
    flex:1
  },
});
