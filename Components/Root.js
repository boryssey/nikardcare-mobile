import React, { useEffect,useState }  from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {connect} from 'react-redux'
import * as Font from 'expo-font';
import { AppLoading, Updates} from 'expo';
import {loginUser} from '../Redux/Actions/AuthActions'
import Home from './Home';
import Login from './Login';
import { NativeRouter, Route, Link } from "react-router-native";
import Questionnaire from './Questionnaire'
import PrivateRoute from './Shared/PrivateRoute'
import deviceStorage from '../utils/deviceStorage';

import { ActionSheetProvider } from '@expo/react-native-action-sheet'


function Root(props) {

  const [isLoadingComplete, setLoadingComplete] = useState(false);

  useEffect(() => {
    deviceStorage.loadTokens().then((data) => {
      if(data.token !== null){
        props.loginUser(data)
      }
    })
  }, [])
  if (!isLoadingComplete && !props.skipLoadingScreen) {
     return (
       <AppLoading
         startAsync={loadResourcesAsync}
         onFinish={() => handleFinishLoading(setLoadingComplete)}
       />
     );
 }
  async function checkUpdates(){
    try {
      const update = await Updates.checkForUpdateAsync();
      if (update.isAvailable) {
        await Updates.fetchUpdateAsync();
        Updates.reloadFromCache();
      }
    } catch (e) {
        console.log('Something went wrong')
    }

  }
  async function loadResourcesAsync() {
    await checkUpdates()
    await Promise.all([
      Font.loadAsync({
            'Source Sans Pro': require('../assets/fonts/SourceSansPro-Regular.ttf'),
            'Source Sans Pro Bold': require('../assets/fonts/SourceSansPro-Bold.ttf'),
            'Source Sans Pro SemiBold': require('../assets/fonts/SourceSansPro-SemiBold.ttf'),
          })
    ]);
  }
  function handleFinishLoading(setLoadingComplete) {
    setLoadingComplete(true);
  }
  return (
      <NativeRouter>
        <ActionSheetProvider>
          <View style={styles.container}>
            <PrivateRoute exact path="/" component={Home} />
            <Route exact path="/login" component={Login} />
            <PrivateRoute exact path="/Questionnaire/:id" component={Questionnaire}/>
          </View>
        </ActionSheetProvider>
      </NativeRouter>

  )
}
const mapStateToProps = state => ({
  isAuth: state.isAuth,
})

const mapDispatchToProps = dispatch => {
  return {
    loginUser: (user) => dispatch(loginUser(user))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Root)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
});
