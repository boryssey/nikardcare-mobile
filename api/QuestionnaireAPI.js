import axios from './index';

export const getQuestionnaire = (patient, idQuestionnaire) => {
  return axios.get(`/patients/${patient}/questionnaires/${idQuestionnaire}`)
}

export const postQuestionnaire = (idQuestionnaire, body) => {
  return axios.post(`/answers/${idQuestionnaire}`, body)
}
