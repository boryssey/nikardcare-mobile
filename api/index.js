import axios from 'axios';

import deviceStorage from '../utils/deviceStorage'
import store from '../Redux/store'
import {logoutUser, refreshToken} from '../Redux/Actions/AuthActions'

const apiAddress = 'https://heart-watch-test.azurewebsites.net/api';
//const apiAddress = 'https://heartwatch-api.boryssey.com/api';
axios.defaults.baseURL = apiAddress;

const refresh = (token, refreshToken) => {
  return axios.post('/identity/refresh',{
    token: token,
    refreshToken: refreshToken
  })
}

axios.interceptors.request.use(

   config => {
     const state = store.getState()
     if (state.user_tokens) {
         config.headers['Authorization'] = 'Bearer ' + state.user_tokens.token;
     }

     return config;
   },
   error => {
       Promise.reject(error)
   });

axios.interceptors.response.use((response) => {console.log('response', response); return response;}, async function (error) {
    const originalReq = error.config;
    let state = store.getState()
    if ((error.response.status === 401 && originalReq.url ===
      apiAddress + '/identity/refresh')) {

        deviceStorage.clearTokens();
        store.dispatch(logoutUser())

        return Promise.reject(error);
    }

    if (error.response.status === 401 && !originalReq._retry) {
        originalReq._retry = true;
        return await refresh(state.user_tokens.token, state.user_tokens.refreshToken).then(res => {
                if (res.status === 200) {
                    deviceStorage.updateToken(res.data.token, res.data.refreshToken)
                    store.dispatch(refreshToken(res.data.token, res.data.refreshToken))
                    axios.defaults.headers.common['Authorization'] = 'Bearer ' + res.data.token;
                  return axios(originalReq);
                }
            })
    }
    return Promise.reject(error);
});
export default axios;
