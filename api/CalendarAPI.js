import axios from './index';

export const getCalendar = (patient, year, month) => {
  return axios.get(`/patients/${patient}/calendar?year=${year}&month=${month}`);
}
