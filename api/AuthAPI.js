import axios from './index';

export const login = (email, password) => {
  return axios.post('/identity/login', {
    email: email,
    password: password
  })
}
export const refresh = (token, refreshToken) => {
  return axios.post('/identity/refresh',{
    token: token,
    refreshToken: refreshToken
  })
}
export const pushToken = (idUser, tokenValue) => {
  return axios.post('/identity/pushToken', {idUser, tokenValue});
}
