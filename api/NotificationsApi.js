import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://heartwatch-push.boryssey.com'
})
export const postToken = (userName, token) => {
  return instance.post('/notifications', {userName, token});
}
