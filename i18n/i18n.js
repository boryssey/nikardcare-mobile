import i18n from 'i18next';
import { initReactI18next  } from 'react-i18next';
import * as Localization from 'expo-localization';
import en from './locales/en-US.json';
import pl from './locales/pl-PL.json';
import Expo from 'expo';

const locales = {
  'en-US': { translation: en },
  'pl-PL': { translation: pl },
};

const languageDetector = {
  type: 'languageDetector',
  async: true, // async detection
  detect: callback => {
    return /*'en'; */ Localization.getLocalizationAsync().then(({ locale }) => {
      callback(locale);
    });
  },
  init: () => {},
  cacheUserLanguage: () => {}
}

i18n
  .use(languageDetector)
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources: locales,
    load: 'all',
    fallbackLng: 'en-US',

    keySeparator: false, // we do not use keys in form messages.welcome

    interpolation: {
      escapeValue: false, // react already safes from xss
    },
  });

export default i18n;
